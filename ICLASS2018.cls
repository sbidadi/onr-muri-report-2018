% This file was converted to LaTeX by Writer2LaTeX ver. 0.4b
% see http://www.hj-gym.dk/~hj/writer2latex for more info
% format is based on the format used for ICLASS 2012 in Heidelberg
\NeedsTeXFormat{LaTeX2e}
% \ProcessOptions\relax
\LoadClass[letter,10pt,oneside]{article}
% \usepackage{babel}
% \usepackage[dvips]{graphicx,color}
\usepackage{graphicx,color}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{amsmath,amssymb,amsfonts,textcomp}
\usepackage{calc}
\usepackage{hyperref}
\usepackage{times}
\hypersetup{colorlinks=false, linkcolor=black, filecolor=black, pagecolor=black, urlcolor=black}
% Text styles
\newcommand\textstyleFootnoteSymbol[1]{#1}
\newcommand\textstyleInternetlink[1]{\textcolor{black}{#1}}
\newcommand{\titlesize}{\fontsize{12}{14}\bfseries\selectfont}
\newcommand{\authorsize}{\fontsize{12}{14}\selectfont}
\renewcommand{\small}{\fontsize{8}{10}\selectfont}
% Outline numbering
\setcounter{secnumdepth}{0}
% List styles
\newcommand\liststyleWWviiiNumii{%
\renewcommand\theenumi{\arabic{enumi}}
\renewcommand\theenumii{\arabic{enumii}}
\renewcommand\theenumiii{\arabic{enumiii}}
\renewcommand\theenumiv{\arabic{enumiv}}
\renewcommand\labelenumi{\theenumi.}
\renewcommand\labelenumii{\theenumii.}
\renewcommand\labelenumiii{\theenumiii.}
\renewcommand\labelenumiv{\theenumiv.}
}
% Pages styles (master pages)
\makeatletter
\newcommand{\runningheads}[2]{\gdef\@runningheads{\small\textit{#1} \hfill \textit{#2}}}
\newcommand{\ps@Standard}{%
\renewcommand{\@oddhead}{\@runningheads}
\renewcommand{\@evenhead}{\@oddhead}%
\renewcommand{\@oddfoot}{\normalfont\hfil\thepage\hfil}%
\renewcommand{\@evenfoot}{\@oddfoot}%
\setlength{\paperwidth}{21cm}
\setlength{\paperheight}{29.7cm}
\setlength{\voffset}{-1in}
\setlength{\hoffset}{-1in}
\setlength{\topmargin}{2cm}
\setlength{\headheight}{12pt}
\setlength{\headsep}{0.5cm}
\setlength{\footskip}{12pt+0.5cm}
\setlength{\textheight}{29.7cm-2cm-2cm-0.5cm-12pt-0.5cm-12pt}
\setlength{\oddsidemargin}{2.5cm}
\setlength{\textwidth}{21cm-2.5cm-2.5cm}
\renewcommand{\thepage}{\arabic{page}}
\setlength{\skip\footins}{0.1cm}
\setlength{\parindent}{0.6cm}
\setlength{\mathindent}{0.6cm}
\renewcommand\footnoterule{\vspace*{-0.018cm}\noindent\textcolor{black}{\rule{0.25\columnwidth}{0.018cm}}\vspace*{0.101cm}}
}


\newcommand{\ps@FirstPage}{%
\renewcommand{\@oddhead}{\hfill\small\textit{ICLASS 2018, 14$^{th}$ Triennial International Conference on Liquid Atomization and Spray Systems, Chicago, IL, USA, July 22-26, 2018}\hfill}%
\renewcommand{\@evenhead}{\@oddhead}%
\renewcommand{\@oddfoot}{\normalfont\hfil\thepage\hfil}%
\renewcommand{\@evenfoot}{\@oddfoot}%
\setlength{\paperwidth}{215.9cm}
\setlength{\paperheight}{279.4cm}
\setlength{\voffset}{-1in}
\setlength{\hoffset}{-1in}
\setlength{\topmargin}{2cm}
\setlength{\headheight}{12pt}
\setlength{\headsep}{0.5cm}
\setlength{\footskip}{12pt+0.5cm}
\setlength{\textheight}{29.7cm-2cm-2cm-0.5cm-12pt-0.5cm-12pt}
\setlength{\oddsidemargin}{2.5cm}
\setlength{\textwidth}{21cm-2.5cm-2.5cm}
\renewcommand{\thepage}{\arabic{page}}
\setlength{\skip\footins}{0.1cm}
\setlength{\parindent}{0.6cm}
\setlength{\mathindent}{0.6cm}
\renewcommand\footnoterule{\vspace*{-0.018cm}\noindent\textcolor{black}{\rule{0.25\columnwidth}{0.018cm}}\vspace*{0.1cm}}
}

% sections
\renewcommand{\section}{\@startsection{section}{1}{\z@}% {1} level {\z@} indent
                                   {0.5cm \@plus -1ex}% -0.5 -> horizontal gap (vertical before section gap when positive)
                                   {0.1ex \@plus.2ex}%
                                   {\normalfont\normalsize\bfseries}}%

\renewcommand{\subsection}{\@startsection{subsection}{2}{\z@}%
                                   {0.5cm \@plus -1ex}%
                                   {0.1ex \@plus.2ex}%
                                   {\normalfont\normalsize\bfseries\em}}%

\renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{\z@}%
                                   {0.5cm \@plus -1ex}%
                                   {.1ex \@plus .2ex}%
                                   {\normalfont\normalsize\bfseries\itshape\hspace{0cm}}}%
% captions
\renewcommand{\@makecaption}[2]{%
 \vskip\abovecaptionskip
 \sbox\@tempboxa{\normalsize\fontsize{10}{12}\selectfont \textbf{#1.} #2}%
 \ifdim \wd\@tempboxa >\hsize
  \normalsize\fontsize{10}{12}\selectfont \textbf{#1.} #2\par
 \else
  \global \@minipagefalse
  \hb@xt@\hsize{\hfil\box\@tempboxa\hfil}%
 \fi
\vskip\belowcaptionskip}

\makeatother
\pagestyle{Standard}
\thispagestyle{FirstPage}
\setlength\tabcolsep{1mm}
\renewcommand{\arraystretch}{2}
% footnotes configuration
\makeatletter
\renewcommand\thefootnote{\normalsize\fnsymbol{footnote}}
\makeatother
\everymath{\displaystyle}


\def\title#1{
\begin{center}
 \vspace*{2pt}
 \titlesize{#1}
 \vspace*{2pt}
\end{center}
}

\def\author#1{
\begin{center}
 \authorsize#1
 \vspace*{2pt}
\end{center}
}

\def\address#1{
\begin{center}
 \vspace*{-12pt}
 \authorsize#1
 \vspace*{-6pt}
\end{center}
}

\setlength{\unitlength}{1cm}
\def\abstract#1{
 \begin{center}
  \normalfont\bfseries{Abstract}
 \end{center}
 \vspace*{-10pt}
 \noindent\normalfont#1{\\}
 \begin{minipage}[t][0.01cm][t]{16cm}
  \begin{picture}(16,0.001)
   \put(0,-0.25){\line(1,0){16}}
  \end{picture}
 \end{minipage}
 \vspace*{0.25cm}
}

\def\thebibliography#1{
\list{{\arabic{enumi}}}
{\def\makelabel##1{\hss{[##1]}}
\topsep=0pt\parsep=0pt\partopsep=0pt\itemsep=0pt
\labelsep=1ex\itemindent=-0.5ex
\settowidth\labelwidth{\footnotesize[#1]}%
\leftmargin\labelwidth
\advance\leftmargin\labelsep
\advance\leftmargin -\itemindent
\usecounter{enumi}}
\def\newblock{\ }
\sloppy\clubpenalty4000\widowpenalty4000
\sfcode`\.=1000\relax
}
\let\endthebibliography=\endlist
